from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer


class AbstractBaseSerializer(ModelSerializer):
    """Handle `id`, `created_by`, `updated_by`, `created`, `updated` and `organization`.

    * when creating a new record, an `id` should not be supplied
    * `created_by`, `organization` and `updated_by` are auto-filled when creating new records
    * `updated_by` is auto-filled when updating new records
    * `organization` is stripped from all requests
    * when creating or updating records, supplied audit fields (`created_by`, `updated_by`,
    `created`, `updated`) are ignored
    """

    def __init__(self, *args, **kwargs):
        """Initialize an audit fields and organization aware serializer."""
        super().__init__(*args, **kwargs)
        exclude_fields = ['created', 'created_by', 'updated', 'updated_by']
        exclude_fields_all_methods = ['organization']
        context = getattr(self, 'context', {})
        request = context.get('request', {})
        request_method = getattr(request, "method", '').lower()
        include_in_methods = ['get', 'head', 'options']
        if request_method not in include_in_methods:
            for i in exclude_fields:
                if i in self.fields:
                    self.fields.pop(i)

        # remove the fields on all views
        for i in exclude_fields_all_methods:
            if i in self.fields:
                self.fields.pop(i)

    def _populate_audit_fields(self, data, is_create):
        request = self.context['request']
        user = request.user
        data['updated_by'] = user.pk

        if is_create:
            data['created_by'] = user.pk
            data['organization'] = user.organization_id
        return data

    def create(self, validated_data):
        """Ensure that audit fields are filled for new records and an `id` cannot be supplied."""
        initial_data_id = (
            isinstance(self.initial_data, dict) and self.initial_data.get('id'))
        if initial_data_id or validated_data.get('id'):
            raise ValidationError({"id": "You are not allowed to pass object with an id"})

        self._populate_audit_fields(validated_data, True)
        return super().create(validated_data)

    def update(self, instance, validated_data):
        """Ensure that update tracking fields are filled in."""
        self._populate_audit_fields(validated_data, False)
        return super().update(instance, validated_data)
