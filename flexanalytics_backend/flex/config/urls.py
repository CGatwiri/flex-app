"""'Main' url configuration for the server."""
from django.conf.urls import include, url

url_patterns = [
    url(
        r'^',
        include(('flex.flex_auth.urls', 'flex_auth'), namespace='flex_auth')
    ),
    url(r'^', include(('flex.uploads.urls', 'uploads'), namespace='uploads')),
]
urlpatterns = [
    url(r'^v1/', include((url_patterns, 'v1'), namespace='v1')),
]
