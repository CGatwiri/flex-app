
from rest_framework import serializers

from flex.common.serializers import AbstractBaseSerializer
from flex.uploads.models import FileUpload


class FileUploadSerializer(AbstractBaseSerializer, serializers.ModelSerializer):
    url = serializers.CharField(read_only=True)

    class Meta(object):
        model = FileUpload
        fields = '__all__'
