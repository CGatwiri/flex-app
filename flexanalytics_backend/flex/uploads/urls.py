from rest_framework.routers import SimpleRouter
from flex.uploads.views import FileUploadViewSet

router = SimpleRouter()
router.register(r'uploads', FileUploadViewSet)
urlpatterns = router.urls
