# Generated by Django 3.0.3 on 2020-02-22 09:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileupload',
            name='upload_type',
            field=models.CharField(choices=[('premium_production', 'Premium production records file'), ('claims_paid', 'Claims paid records file'), ('claims_outstanding', 'Claims outstanding records file')], max_length=255),
        ),
    ]
