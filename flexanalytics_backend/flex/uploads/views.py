
from rest_framework import viewsets, parsers

from flex.uploads.models import FileUpload
from flex.uploads.serializers import FileUploadSerializer


class FileUploadViewSet(viewsets.ModelViewSet):
    """File attachments view."""

    queryset = FileUpload.objects.all()
    serializer_class = FileUploadSerializer
    parser_classes = (parsers.MultiPartParser, parsers.JSONParser)
