import string
import random
import datetime

from django.conf import settings
from django.db import models
from django.core.exceptions import ValidationError

from flex.common.models import AbstractBase


UPLOAD_TYPES = (
    ('premium_production', 'Premium production records file'),
    ('claims_paid', 'Claims paid records file'),
    ('claims_outstanding', 'Claims outstanding records file'),
)


def rand_gen(size=12):
    chars = string.ascii_uppercase + string.digits
    return ''.join(random.choice(chars) for _ in range(size))


def upload(instance, filename):
    target_dir = settings.MEDIA_ROOT
    instance.name = filename
    suffix = rand_gen() + '.' + filename.split('.')[-1]
    time_stamp = datetime.datetime.now().strftime("%Y/%m/%d/%H-%M-%S-%f")
    return "{0}/{1}_{2}".format(target_dir, time_stamp, suffix)


class FileUpload(AbstractBase):
    upload_type = models.CharField(max_length=255, choices=UPLOAD_TYPES)
    name = models.CharField(max_length=255)
    uploaded_file = models.FileField(upload_to=upload)

    @property
    def url(self):
        url = self.uploaded_file.url.replace(settings.MEDIA_ROOT, '')
        return url if '://' in url else settings.MEDIA_URL + url

    def validate_upload_type(self):
        if self.upload_type not in [option[0] for option in UPLOAD_TYPES]:
            raise ValidationError(
                {
                    'upload_type': [
                        'The upload type provided is not a valid option'
                    ]
                }
            )

    def clean(self):
        self.validate_upload_type()
        super(FileUpload, self).clean()

    def save(self, *args, **kwargs):
        self.full_clean(exclude=None)
        super(FileUpload, self).save(*args, **kwargs)
