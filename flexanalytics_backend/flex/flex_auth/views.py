from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from . import filters
from . import models
from . import serializers

class APIRoot(APIView):

    """
    Welcome to FlexAnalytics API.

    /uploads/file_uploads/
    """

    def get(self, request, format=None):
        return Response()


class GroupViewSet(ModelViewSet):
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    filter_class = filters.GroupFilter


class PermissionViewSet(ModelViewSet):
    queryset = models.Permission.objects.all()
    serializer_class = serializers.PermissionSerializer
    filter_class = filters.PermissionFilter


class GroupPermissionViewSet(ModelViewSet):
    queryset = models.GroupPermission.objects.all()
    serializer_class = serializers.GroupPermissionSerializer
    filter_class = filters.GroupPermissionFilter


class OrganizationViewSet(ModelViewSet):
    queryset = models.Organization.objects.all()
    serializer_class = serializers.OrganizationSerializer
    filter_class = filters.OrganizationFilter


class FlexUserViewSet(ModelViewSet):
    queryset = models.FlexUser.objects.all()
    serializer_class = serializers.FlexUserSerializer
    filter_class = filters.FlexUserFilter


class FlexOauthApplicationViewSet(ModelViewSet):
    queryset = models.FlexOauthApplication.objects.all()
    serializer_class = serializers.FlexOauthApplicationSerializer


class MeView(RetrieveUpdateAPIView):
    queryset = None
    serializer_class = serializers.FlexUserSerializer

    def get_object(self):
        return self.request.user
