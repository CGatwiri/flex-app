import django_filters
from . import models


class FlexUserFilter(django_filters.FilterSet):

    class Meta:
        model = models.FlexUser
        fields = ['email', 'organization']


class PermissionFilter(django_filters.FilterSet):

    class Meta:
        model = models.Permission
        fields = ['name']


class GroupFilter(django_filters.FilterSet):

    class Meta:
        model = models.Group
        fields = '__all__'


class GroupPermissionFilter(django_filters.FilterSet):

    class Meta:
        model = models.GroupPermission
        fields = '__all__'


class OrganizationFilter(django_filters.FilterSet):

    class Meta:
        model = models.Organization
        fields = '__all__'
