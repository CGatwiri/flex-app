from django.apps import AppConfig


class FlexAuthConfig(AppConfig):
    name = 'flex_auth'
