import uuid
from rest_framework import serializers

from . import models


class GroupSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.Group
        fields = '__all__'


class PermissionSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.Permission
        fields = '__all__'


class GroupPermissionSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.GroupPermission
        fields = '__all__'


class OrganizationSerializer(serializers.ModelSerializer):
    subsidiary_count = serializers.ReadOnlyField()

    class Meta(object):
        model = models.Organization
        fields = '__all__'


class FlexUserSerializer(serializers.ModelSerializer):
    permission_name = serializers.ReadOnlyField(source='permission.name')
    group_name = serializers.ReadOnlyField()
    company_name = serializers.ReadOnlyField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('password')

    def create(self, validated_data):
        validated_data['password'] = str(uuid.uuid4()).replace('-', '')[0:10]
        return models.FlexUser.objects.create_user(**validated_data)

    class Meta(object):
        model = models.FlexUser
        fields = '__all__'


class FlexOauthApplicationSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.FlexOauthApplication
        fields = '__all__'
