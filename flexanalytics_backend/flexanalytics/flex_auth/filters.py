import django_filters
from . import models


class FlexAnalyticsUserFilter(django_filters.FilterSet):

    class Meta:
        model = models.FlexAnalyticsUser
        fields = ['email', 'company']


class PermissionFilter(django_filters.FilterSet):

    class Meta:
        model = models.Permission
        fields = ['name']


class GroupFilter(django_filters.FilterSet):

    class Meta:
        model = models.Group
        fields = '__all__'


class GroupPermissionFilter(django_filters.FilterSet):

    class Meta:
        model = models.GroupPermission
        fields = '__all__'


class CompanyFilter(django_filters.FilterSet):

    class Meta:
        model = models.Company
        fields = '__all__'


class CompanySubsidiaryFilter(django_filters.FilterSet):

    class Meta:
        model = models.CompanySubsidiary
        fields = '__all__'
