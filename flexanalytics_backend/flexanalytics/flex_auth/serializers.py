import uuid
from rest_framework import serializers

from . import models


class GroupSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.Group
        fields = '__all__'


class PermissionSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.Permission
        fields = '__all__'


class GroupPermissionSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.GroupPermission
        fields = '__all__'


class CompanySerializer(serializers.ModelSerializer):
    subsidiary_count = serializers.ReadOnlyField()

    class Meta(object):
        model = models.Company
        fields = '__all__'


class CompanySubsidiarySerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.CompanySubsidiary
        fields = '__all__'


class FlexAnalyticsUserSerializer(serializers.ModelSerializer):
    permission_name = serializers.ReadOnlyField(source='permission.name')
    group_name = serializers.ReadOnlyField()
    company_name = serializers.ReadOnlyField()
    company_subsidiary_name = serializers.ReadOnlyField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('password')

    def create(self, validated_data):
        validated_data['password'] = str(uuid.uuid4()).replace('-', '')[0:10]
        return models.SolomonUser.objects.create_user(**validated_data)

    class Meta(object):
        model = models.FlexAnalytics
        fields = '__all__'


class FlexOauthApplicationSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = models.FlexOauthApplication
        fields = '__all__'
