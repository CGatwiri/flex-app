from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from . import filters
from . import models
from . import serializers

class APIRoot(APIView):

    """
    Welcome to FlexAnalytics API.

    /integration/data_preparation/
    /integration/reports/
    /integration/forecast/
    """

    def get(self, request, format=None):
        return Response()


class GroupViewSet(ModelViewSet):
    queryset = models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    filter_class = filters.GroupFilter


class PermissionViewSet(ModelViewSet):
    queryset = models.Permission.objects.all()
    serializer_class = serializers.PermissionSerializer
    filter_class = filters.PermissionFilter


class GroupPermissionViewSet(ModelViewSet):
    queryset = models.GroupPermission.objects.all()
    serializer_class = serializers.GroupPermissionSerializer
    filter_class = filters.GroupPermissionFilter


class CompanyViewSet(ModelViewSet):
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerializer
    filter_class = filters.CompanyFilter


class CompanySubsidiaryViewSet(ModelViewSet):
    queryset = models.CompanySubsidiary.objects.all()
    serializer_class = serializers.CompanySubsidiarySerializer
    filter_class = filters.CompanySubsidiaryFilter


class FlexAnalyticsUserViewSet(ModelViewSet):
    queryset = models.FlexAnalyticsUser.objects.all()
    serializer_class = serializers.FlexAnalyticsUserSerializer
    filter_class = filters.FlexAnalyticsUserFilter


class FlexOauthApplicationViewSet(ModelViewSet):
    queryset = models.FlexOauthApplication.objects.all()
    serializer_class = serializers.FlexOauthApplicationSerializer


class MeView(RetrieveUpdateAPIView):
    queryset = None
    serializer_class = serializers.SolomonUserSerializer

    def get_object(self):
        return self.request.user
