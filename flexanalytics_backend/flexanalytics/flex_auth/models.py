import uuid
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils import timezone
from oauth2_provider.models import AbstractApplication
from django.template import Context, loader
from django.core.mail import EmailMultiAlternatives


def send_email(user, password):
    html_template = loader.get_template(
        "registration/success.html")
    context = Context(
        {
            "user": user,
            "password": password,
            "login_url": settings.FRONTEND_URL
        }
    )
    plain_text = loader.get_template("registration/success.txt")
    subject = "FlexAnalytics Account Created"
    text_content = plain_text.render(context)
    html_content = html_template.render(context)
    email = settings.EMAIL_HOST_USER
    msg = EmailMultiAlternatives(
        subject, text_content, email, [user.email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


class FlexAnalyticsUserManager(BaseUserManager):

    def create_user(self, email, first_name, last_name,
                    company, group, password=None,
                    **extra_fields):
        now = timezone.now()
        p = make_password(password)
        email = FlexAnalyticsUserManager.normalize_email(email)
        user = self.model(
            email=email, first_name=first_name, password=p,
            company=company, group=group, last_name=last_name,
            is_active=True, is_superuser=False, date_joined=now,
            **extra_fields
        )
        user.save(using=self._db)
        send_email(user, password)
        return user

    def create_superuser(self, email, first_name, last_name,
                         company, group, password,
                         **extra_fields):
        user = self.create_user(
            email, first_name, last_name, company, group,
            password, **extra_fields)
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Group(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)
    status = models.BooleanField()


class Permission(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)

    @classmethod
    def default_permissions(self):
        perms = [
            "CAN_WORKON_DATASTORE",
            "CAN_WORKON_DATA_MANIPULATION",
            "CAN_WORKON_REPORTS",
            "CAN_WORKON_BUDGETING",
            "CAN_WORKON_ADMIN",
        ]
        for perm in perms:
            try:
                self.objects.get(name=perm)
            except self.DoesNotExist:
                self.objects.create(name=perm)

    def __str__(self):
        return self.name


class GroupPermission(models.Model):
    group = models.ForeignKey(
        Group, on_delete=models.PROTECT, related_name='permissions'
    )
    permission = models.ForeignKey(Permission, on_delete=models.PROTECT)


class Company(models.Model):
    name = models.CharField(max_length=255, unique=True)

    @property
    def subsidiary_count(self):
        return CompanySubsidiary.objects.filter(company=self).count()


class CompanySubsidiary(models.Model):
    name = models.CharField(max_length=255, unique=True)
    company = models.ForeignKey(
        Company, on_delete=models.PROTECT,related_name='company_subsidiaries'
    )


class FlexAnalyticsUser(AbstractBaseUser):
    USERNAME_FIELD = 'email'
    group = models.ForeignKey(Group, on_delete=models.PROTECT)
    company = models.ForeignKey(
        Company, on_delete=models.PROTECT, related_name='company_users'
    )
    company_subsidiary = models.ForeignKey(
        CompanySubsidiary, null=True, blank=True, on_delete=models.PROTECT,
        related_name='subsidiary_users'
    )
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    is_superuser = models.BooleanField(default=True)

    objects = FlexAnalyticsUserManager()

    @property
    def permissions(self):
        return [
            gp.permission.name for gp in GroupPermission.objects.filter(group=self.group)
        ]

    @property
    def names(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def group_name(self):
        return self.group.name

    @property
    def company_name(self):
        return self.company.name

    @property
    def company_subsidiary_name(self):
        return self.company_subsidiary.name if self.company_subsidiary else ''



class FlexOauthApplication(AbstractApplication):

    class Meta(object):
        verbose_name = 'flex oauth application'
