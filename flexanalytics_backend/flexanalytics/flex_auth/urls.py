from django.conf.urls import url
from rest_framework.routers import SimpleRouter

from . import views

app_name = 'flex_auth'

router = SimpleRouter()

router.register(r'users', views.FlexAnalyticsUserViewSet)
router.register(r'permissions', views.PermissionViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'group_permissions', views.GroupPermissionViewSet)
router.register(r'companies', views.CompanyViewSet)
router.register(r'subsidiaries', views.CompanySubsidiaryViewSet)
router.register(r'applications', views.FlexOauthApplicationViewSet)

urlpatterns = router.urls
urlpatterns += url(r'^me/$', views.MeView.as_view(), name='me'),
