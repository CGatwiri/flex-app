from django.shortcuts import render
from django.db import models
from rest_framework import viewsets
from flexanalytics.premiums.models import Premiums_production
from flexanalytics.claims.models import Claims_paid,ClaimsOutstanding
from .serializers import PremsumSerializer
from django.db.models import Q
from rest_framework.views import APIView
from io import StringIO
import xlsxwriter
from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest, HttpResponse
from _compact import JsonResponse
import django_excel as excel
import pandas as pd


def export_data(request):
    
    serializer_class = PremsumSerializer
    firstnames = Premiums_production.objects.values_list('Branch', flat=True).distinct()
    firstnames = tuple(firstnames)

    num = len(firstnames)

    writer = pd.ExcelWriter('PremiumSummaries.xlsx', engine='xlsxwriter')

    for i in firstnames:
        queryset = Premiums_production.objects.filter(
            Q(AccountYear='2005')|Q(AccountYear='2004'),Q(Branch=i))
        q = queryset.values('Class','Agency','SumInsured','BasicPrem','AccountYear','Branch')
        df = pd.DataFrame.from_records(q)
        df.to_excel(writer, sheet_name=i)

    writer.save()

    return HttpResponse()

def prem_sum(request):
    
    # serializer_class = PremsumSerializer
    firstnames = Premiums_production.objects.values_list('Branch', flat=True).distinct()
    classes = Premiums_production.objects.values_list('Class', flat=True).distinct()
    years = Premiums_production.objects.values_list('AccountYear', flat=True).distinct()
    firstnames = tuple(firstnames)
    classes = tuple(classes)
    years = tuple(years)

    num = len(firstnames)

    writer = pd.ExcelWriter('UapPremSummaries.xlsx', engine='xlsxwriter')

    for i in firstnames:
        thisqueryset = Premiums_production.objects.all().filter(Q(Branch=i),Q(Insured="UAP HOLDINGS LTD"))
        df = pd.DataFrame(thisqueryset.values())
        print(df)
        df = pd.DataFrame(columns=classes,index=years)
        # df.set_axis([years],axis='index')
        df.to_excel(writer, sheet_name=i)
        # df.set_index('AccountYear')
        

    writer.save()

    return HttpResponse()
     
