from django.urls import path, include, re_path
from rest_framework import routers
from .import views
# from views import export_data

router = routers.DefaultRouter()

# router.register(r'^export/(.*)', export_data)

urlpatterns = [
    re_path(r'^export', views.export_data, name="export"),
    re_path(r'^sum', views.prem_sum, name="sum"),

 ]
# urlpatterns = [
#     path('', include(router.urls)),
# ]
