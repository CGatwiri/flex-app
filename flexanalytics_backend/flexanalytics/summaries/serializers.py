from rest_framework import serializers
from django.db import models
from rest_framework import viewsets
from flexanalytics.premiums.models import Premiums_production

class PremsumSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Premiums_production
        fields = 'Class','Agency','SumInsured','BasicPrem','AccountYear'
