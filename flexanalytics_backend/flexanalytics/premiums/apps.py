from django.apps import AppConfig


class PremiumsConfig(AppConfig):
    name = 'premiums'
