from django.urls import include, path
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    path('api/', include('rest_framework.urls', namespace='rest_framework')),
    path('uploads/', include('flexanalytics.file_upload.urls')),
    path('summaries/', include('flexanalytics.summaries.urls')),
    # path('users/', include('flexanalytics.authentication.urls')),
    # url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    # path('account/', include('django.contrib.auth.urls')),



]
