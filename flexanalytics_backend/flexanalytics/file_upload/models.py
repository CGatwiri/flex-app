from django.db import models
import os

# Create your models here.
def directory_path(instance, filename):
    return os.path.join("uploads",instance.filetype, filename)

class FileUpload(models.Model):
    filetype = models.CharField(max_length=150,blank=False)
    title = models.FileField(upload_to=directory_path)
