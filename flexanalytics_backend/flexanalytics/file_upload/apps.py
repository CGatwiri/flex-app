from django.apps import AppConfig


class FileuploadConfig(AppConfig):
    name = 'flexanalytics.file_upload'

    def ready(self):
        # everytime server restarts it does this
        import flexanalytics.file_upload.signals
