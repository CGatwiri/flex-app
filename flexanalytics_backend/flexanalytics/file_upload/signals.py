from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver
from .models import FileUpload
from sqlalchemy import create_engine
import pandas as pd
import glob
import os

# this receiver is executed every-time some data is saved in Claims paid table
@receiver(post_save, sender=FileUpload)
def claims_paid_extraction(sender, instance, created, **kwargs):
    #Check whether the file belongs to Claims Paid table

    if created:
        new_file = instance.title
        split_name_of_file = (str(new_file).split('/'))
        name_of_file =split_name_of_file[2]
        path = 'G:\\Clydan\\Clydan\\flexanalytics_backend\\flexanalytics\\media\\uploads\\Claims Paid\\' + name_of_file

        if os.path.isfile(path):
            # code to execute after every model save
            def claims_paid_upload():
                # Reading the dataset
                df = pd.read_csv(path, sep=',')
                # null_data = df[df.isnull().any(axis=1)]
                # def directory_path(instance, filename):
                #     return os.path.join("MissingValuesFiles", instance.filetype, filename)
                # error_file = null_data.to_csv(directory_path)

                raw_data = df.loc[:,
                           ['Branch', 'ClaimNo', 'PolicyNo', 'Insured', 'Payee', 'DOLA', 'PayeeType', 'TransType', 'PayDate',
                            'ClassCode', 'RegNo', 'CurrentDOLA', 'ClaimsStatus',
                            'ClassDescr', 'GlClass', 'Agency', 'Source', 'AgencyCode', 'LossDate', 'NotifiedDate', 'CauseCode',
                            'Cause', 'LivesLost', 'NoInjured', 'EffDate', 'PeriodFrom',
                            'PeriodTo', 'CreatedBy', 'SumInsured', 'GrossAmount', 'MandatoryShare', 'CompanyShare',
                            'FirstXlossShare', 'SecondXlossShare', 'ThirdXlossShare',
                            'QuotaShare', 'FirstSurplusShare', 'SecondSurplusShare', 'ThirdSurplusShare', 'FacultativeShare',
                            'CurrentEstimate']]
                DB_CONNECTION_URL = create_engine('postgres+psycopg2://postgres:destiny,123@localhost:5432/flex',
                                                  echo=True, pool_pre_ping=True)
                raw_data.to_sql('claims_claims_paid', con=DB_CONNECTION_URL, if_exists='append', chunksize=1000)


            claims_paid_upload()
            print("Claims Paid Data extracted successfully")


# this receiver is executed every-time some data is saved in claimsoutstanding table
@receiver(post_save, sender=FileUpload)
def claims_outstanding_extraction(sender, instance, created, **kwargs):
    # Check whether the file belongs to Claims Paid table
    if created:
        new_file = instance.title
        split_name_of_file = (str(new_file).split('/'))
        name_of_file = split_name_of_file[2]
        path = 'G:\\Clydan\\Clydan\\flexanalytics_backend\\flexanalytics\\media\\uploads\\ClaimsOutstanding\\' + name_of_file

        if os.path.isfile(path):
            # code to execute after every model save
            def claimsoutstanding_upload():
                # Reading the dataset
                df = pd.read_csv(path, sep=',')
                # null_data = df[df.isnull().any(axis=1)]
                # def directory_path(instance, filename):
                #     return os.path.join("MissingValuesFiles", instance.filetype, filename)
                # error_file = null_data.to_csv(directory_path)

                raw_data = df.loc[:,
                           ['Branch', 'PolicyNo', 'UwYear', 'AccDate', 'NotifiedDate', 'ClaimNo', 'Class', 'GlClass', 'Agency',
                            'Source', 'Insured', 'GrossReserve',
                            'MandatoryClaim', 'QuotaShareClaim', 'SurplusFirstClaim', 'SurplusSecondClaim', 'SurplusThirdClaim',
                            'FacultativeClaim', 'XlossClaim', 'NetOutstanding', 'GrossPaidTodate']]
                DB_CONNECTION_URL = create_engine('postgres+psycopg2://postgres:destiny,123@localhost:5432/flex',
                                                  echo=True, pool_pre_ping=True)
                raw_data.to_sql('claims_claimsoutstanding', con=DB_CONNECTION_URL, if_exists='append', chunksize=1000)


            claimsoutstanding_upload()
            print("Claims Outstanding Data extracted successfully")

# this receiver is executed every-time some data is saved in the premiums production table
@receiver(post_save, sender=FileUpload)
def premiums_production_extraction(sender, instance, created,  **kwargs):
    # Check whether the file belongs to Claims Paid table
    if created:
         new_file = instance.title
         split_name_of_file = (str(new_file).split('/'))
         name_of_file = split_name_of_file[2]
         path = 'G:\\Clydan\\Clydan\\flexanalytics_backend\\flexanalytics\\media\\uploads\\Premiums Production\\' + name_of_file
#/home/christinegatwiri18/flexanalytics_backend/Premiums Production
         if os.path.isfile(path):
             # code to execute after every model save
             def premiums_production_upload():
                # Reading the dataset
                df = pd.read_csv(path, sep=',')
                null_data = df[df.isnull().any(axis=1)]
                # def directory_path(instance, filename):
                #     return os.path.join("MissingValuesFiles", instance.file_type, filename)
                # error_file = null_data.to_csv(directory_path)
                #highlighted_missing_data = null_data.style.applymap(lambda x: 'color: red' if pd.isnull(x) else '')

                #error_file = highlighted_missing_data.to_csv("PremiumsProductionMissingvalues.csv")

                raw_data = df.loc[:,
                               ['Branch', 'PolicyNo', 'Reference', 'Insured', 'InsuredPartnerNo', 'EndorsementNumber', 'DocType',
                                'EntryType', 'TransDate',
                                'NewRenewal', 'EffectiveDate', 'PeriodFrom', 'PeriodTo', 'AccountYear', 'AccountMonth', 'GlClass',
                                'ClassCode', 'Class', 'Agency',
                                'AgencyPartnerNo', 'Source', 'BusinessType', 'BookedBy', 'Currency', 'Rate', 'DolaDate',
                                'ClientType', 'AgencyCode',
                                'AdminFee', 'SumInsured', 'BasicPrem', 'MakaoSalamaPrem', 'GrossComm', 'ForeignPremium',
                                'CommissionRate', 'IntegratedCommission',
                                'AXAPrem', 'MandatoryPrem', 'QuotaPrem', 'QuotaComm', 'SurplusPrem', 'SurplusComm', 'FacultPrem',
                                'FacultComm',
                                'NetPremium', 'XOL1', 'XOL2', 'XOL3', 'XOL4', 'CAT1', 'CAT2', 'CAT3', 'AbsNetPremium',
                                'QuakePremium']]
                DB_CONNECTION_URL = create_engine('postgres+psycopg2://postgres:destiny,123@localhost:5432/flex',
                                                      echo=True, pool_pre_ping=True)
                raw_data.to_sql('premiums_premiums_production', con=DB_CONNECTION_URL, if_exists='append', chunksize=10000)

             premiums_production_upload()
             print("Premiums Production Data extracted successfully")