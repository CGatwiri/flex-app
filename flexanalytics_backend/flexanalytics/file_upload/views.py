from django.shortcuts import render
from django.shortcuts import render
from rest_framework import viewsets

from .models import FileUpload
from .serializers import FileUploadSerializer
# Create your views here.

class FileUpload_View_Set(viewsets.ModelViewSet):
    queryset =FileUpload.objects.all()
    serializer_class = FileUploadSerializer

