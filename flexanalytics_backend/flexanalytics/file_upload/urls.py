from django.urls import path, include
from rest_framework import routers
from .import views


router = routers.DefaultRouter()

router.register(r'file_upload', views.FileUpload_View_Set)

urlpatterns = [
    path('', include(router.urls)),
]
