from setuptools import setup, find_packages

VERSION = '0.0.1'

with open('README.md') as readme:
    README = readme.read()

setup(
    name='flexanalytics',
    version=VERSION,
    description='flexanalytics',
    long_description=README,
    author='',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'django==3.0.6',
        'django-filter==2.2.0',
        'djangorestframework==3.11.0',
        'django-cors-headers==3.3.0',
        'django-oauth-toolkit==1.3.2',
        'drf-renderer-xlsx==0.3.7',
        'pandas==1.0.3',
        'psycopg2==2.8.5',
        'SQLAlchemy==1.3.17'
    ],
    include_package_data=True,
)
